﻿using Entitas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class AddGameObjectSystem : IReactiveSystem {
    public TriggerOnEvent trigger { get { return Matcher.Resource.OnEntityAdded(); } }    

    public void Execute(List<Entity> entities) {
        foreach (var e in entities) {     
            GameObject instance = InstantiatePrefab(e.resource.Name);
            if (instance != null) {
                if (e.hasGameObject) {                    
                    UnityEngine.Object.Destroy(e.gameObject.obj);                    
                }

                EntityContainer c = instance.AddComponent<EntityContainer>();
                c.Entity = e;
                e.ReplaceGameObject(instance);               
            }
        }
    }

    public static GameObject InstantiatePrefab(string path) {        
        try {            
            var res = Resources.Load<GameObject>(path);
            return UnityEngine.Object.Instantiate(res);
        } catch (Exception) {
            Debug.LogError(String.Format("Cannot load prefab at '{0}'", path));            
            return null;
        }
    }
}