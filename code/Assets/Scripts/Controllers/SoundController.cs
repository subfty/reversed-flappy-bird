﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundController : Singleton<SoundController> {

    public enum AudioSources {
        SoundEffects,
        Loops
    }

    public enum Sounds {
        ButtonClick, // done
        LockedButtonClick, // done
        ButtonUnlock, // done
        ButtonLock, // done
        PlayerCollision, // done
        MenuButtonClick, // done
        MenuButtonAppear, // done
        EnemyHit, // done
        AttackFailed, // done
        FightWon, // done
        DialogTypewriterSound, // done
        TerminalTypewriterSound, // done
        ChargePress, // done
        VisionBooted, // done
        TransmiterSound,
        FloorButtonClick,
        DoorsSoundEnter, // done
        DoorsSoundExit, // done
        HoloDeckChamberEnter,
        HoloDeckChamberExit,
        TestStageCompleted,
        ItemReceived, // done
        MenuSlideIn,
        FightModeOptionSelected, // done
        NPCButtonPressed, //done
        FightModeEnter, //done
        PlayerHit, // done
        PlayerStep, // done


        None = 9999
    }    

    public enum Tunes {

    }

    [SerializeField]
    public List<AudioSource> _Sources = new List<AudioSource>();

    [SerializeField]
    public List<float> _SourcesBaseVolumes = new List<float>();

    [SerializeField]
    [HideInInspector]
    public List<AudioClip> _AudioClips = new List<AudioClip>();

    public bool SoundEnabled = true;

	void Start () {
        TinyTokenManager.Instance.Register<Msg.PauseSystemExecution>(this, (m) => {
            StopPlayingAllSounds();
        });
    }

    void Stop() {
        TinyTokenManager.Instance.UnregisterAll(this);
    }

    public void PlaySound(Sounds sound, float volume = 1.0f) {
        if (!SoundEnabled) return;
        if (_AudioClips.Count > (int)sound && _AudioClips[(int)sound] != null) {
            _Sources[(int)AudioSources.SoundEffects].PlayOneShot(_AudioClips[(int)sound], volume * _SourcesBaseVolumes[(int)sound]);
        }
    }

    public void PlaySoundRepeatedly(Sounds sound, float volume = 1.0f) {        
        if (!SoundEnabled) return;
        if (_AudioClips.Count > (int)sound && _AudioClips[(int)sound] != null) {
            _Sources[(int)AudioSources.Loops].volume = volume * _SourcesBaseVolumes[(int)sound];
            _Sources[(int)AudioSources.Loops].clip = _AudioClips[(int)sound];
            _Sources[(int)AudioSources.Loops].loop = true;
            _Sources[(int)AudioSources.Loops].Play();
        }        
    }

    public void StopPlayingSoundRepeatedly(Sounds sound) {        
        if (!SoundEnabled) return;
        if (_AudioClips.Count > (int)sound && _AudioClips[(int)sound] != null) {
            if (_Sources[(int)AudioSources.Loops].clip == _AudioClips[(int)sound]) {
                _Sources[(int)AudioSources.Loops].Stop();
                _Sources[(int)AudioSources.Loops].clip = null;
            }
        }
    }

    public void StopPlayingAllSounds() {
        // TODO: implement
        Debug.Log(string.Format("Stop all sounds"));
        
        if (_Sources[(int)AudioSources.Loops].clip != null) {
            _Sources[(int)AudioSources.Loops].Stop();
            _Sources[(int)AudioSources.Loops].clip = null;
        }        
    }

    public void PlayTune(Tunes tune, float volume = 1.0f) {
        if (!SoundEnabled) return;
        Debug.Log(string.Format("Playing tune: {0} with volume {1}", tune, volume));
    }
}
