﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class ReversedFlappyBirdPipe : MonoBehaviour {    
    [SerializeField]
    protected RectTransform _TopPart;
    [SerializeField]
    protected RectTransform _BotomPart;

    CanvasGroup _CanvasGroup;
    RectTransform _RectTransform;

    public float DotYVelocity;

    public RectTransform RTransform {
        get { 
            if(_RectTransform == null)
                _RectTransform = GetComponent<RectTransform>();
            return _RectTransform; 
        }
        private set { }
    }

    public CanvasGroup Group {
        get {
            if (_CanvasGroup == null)
                _CanvasGroup = GetComponent<CanvasGroup>();
            return _CanvasGroup;
        }
        private set { }
    }

    protected void Awake() {        
        
    }

    public bool IsColliding(Vector2 dotAnchoredPosition) {
        Rect topRect = new Rect(
            _TopPart.anchoredPosition.x,
            _TopPart.anchoredPosition.y,
            _TopPart.sizeDelta.x,
            _TopPart.sizeDelta.y);
        Rect bottomRect = new Rect(
            _BotomPart.anchoredPosition.x,
            _BotomPart.anchoredPosition.y,
            _BotomPart.sizeDelta.x,
            _BotomPart.sizeDelta.y);

        return 
            topRect.Contains(dotAnchoredPosition) ||
            bottomRect.Contains(dotAnchoredPosition);
    }

    public void SetGapSize(float height) {
        _BotomPart.anchoredPosition = new Vector2(
            _BotomPart.anchoredPosition.x,
            (FightPlaceholder.PLAY_AREA_HEIGHT - height) * 0.5f);
        _TopPart.anchoredPosition = new Vector2(
            _TopPart.anchoredPosition.x,
            (FightPlaceholder.PLAY_AREA_HEIGHT + height) * 0.5f);
    }
}
