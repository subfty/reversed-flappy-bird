﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ReversedFlappyBird : FightPlaceholder {
    #region Game Object bindings
    [Header("Resources")]
    [SerializeField]
    private GameObject _Resources;
    [SerializeField]
    private ReversedFlappyBirdPipe _ObstaclePrefab;
    #endregion

    #region Config
    [Header("Config")]
    [SerializeField]    
    private DiffRangeProperty _ObstacleSpeed = new DiffRangeProperty(250.0f, 150.0f);
    [SerializeField]    
    private DiffRangeProperty _BaseSpawnDelay = new DiffRangeProperty(2.0f, -1.0f);
    [SerializeField]
    [Range(0, 1)]
    private float _MaxSpawnDelayVariance = 0.2f;
    [SerializeField]    
    DiffRangeProperty _JumpAcc = new DiffRangeProperty(0.1f, 0.7f); //0.8
    [SerializeField]    
    DiffRangeProperty _Gravity = new DiffRangeProperty(0.8f, 0.7f);
    
    [SerializeField]    
    DiffRangeProperty _MinHoleSize = new DiffRangeProperty(144, 0); // 144
    [SerializeField]
    DiffRangeProperty _MaxHoleSize = new DiffRangeProperty(207, 0); // 207    

    [Header("Bird Confid")]
    [SerializeField]
    [Range(0, FightPlaceholder.PLAY_AREA_WIDTH)]
    float _StartBirdPosX = 100;
    [SerializeField]
    float _StartBirdPosY = 300;
    [SerializeField]
    float _BirdDeltaY = 2.0f;
    [SerializeField]
    float _BirdSineSpeed = 0.5f;
    #endregion   

    #region Properties
    float ObstacleSpeed {
        get { return _ObstacleSpeed.GetValue(GameDifficulty); }
    }

    float SpawnTime {
        get { return _SpawnDelay; }
    }    

    float DotRadius {
        get { return 20.0f; }
    }

    float Gravity {
        get { return -_Gravity.GetValue(GameDifficulty) * 1000.0f; }
    }

    float JumpAcc {
        get {
            return _JumpAcc.GetValue(GameDifficulty) * 1000.0f;
        }
    }
    #endregion

    #region Pools
    List<ReversedFlappyBirdPipe> _ObstaclesPool;
    #endregion
    
    FightDot _Dot;
    RectTransform _TDot;    
    float _LastSpawned;
    float _SpawnDelay;
    float _GameDuration;

    void RandNewSpawnDelay() {
        _SpawnDelay = 
            _BaseSpawnDelay.GetValue(GameDifficulty) + 
            _MaxSpawnDelayVariance * UnityEngine.Random.Range(0.0f, 1.0f);
    }

    protected override void Start() {
        base.Start();

        // initialisation
        if (_Dot == null) {
            _Resources.gameObject.SetActive(false);

            _Dot = SpawnDot();
            _Dot.transform.SetParent(this.transform, false);
            _TDot = _Dot.GetComponent<RectTransform>();

            _ObstaclesPool = new List<ReversedFlappyBirdPipe>();
        }

        if (_GemTypes != null)
            _Dot.SetGemTypes(_GemTypes);

        _TDot.anchoredPosition = new Vector2(
            _StartBirdPosX, 
            _StartBirdPosY);

        _GameDuration = 0.0f;
    }

    protected override void OnDestroy() {
        base.OnDestroy();

        RecyclePool(_ObstaclesPool);
    }

    protected override void GameUpdate() {
        base.GameUpdate();

        float delta = Time.deltaTime;

        _GameDuration += delta;

        // moving obstacles
        foreach (ReversedFlappyBirdPipe pipe in _ObstaclesPool) {
            if (!pipe.gameObject.activeSelf) continue;            
            
            Vector2 newPipePosition = new Vector2(
                pipe.RTransform.anchoredPosition.x - delta * ObstacleSpeed,
                pipe.RTransform.anchoredPosition.y);

            if (JustPressed) {
                pipe.DotYVelocity = JumpAcc;                
            }

            pipe.DotYVelocity += Gravity * delta;
            newPipePosition += new Vector2(0, delta * pipe.DotYVelocity);

            pipe.RTransform.anchoredPosition = newPipePosition;

            if (pipe.RTransform.anchoredPosition.x + pipe.RTransform.sizeDelta.x < 0)
                Recycle<ReversedFlappyBirdPipe>(pipe);
        }

        // TODO: spawning obstacles    
        // spawning obstacles
        _LastSpawned += delta;
        while (_LastSpawned > SpawnTime) {
            _LastSpawned -= SpawnTime;
                        
            ReversedFlappyBirdPipe pipe = GetRecycled(_ObstaclesPool, ObstacleFactory).GetComponent<ReversedFlappyBirdPipe>();
            ObstacleInit(pipe, FightPlaceholder.PLAY_AREA_WIDTH, FightPlaceholder.PLAY_AREA_HEIGHT / 2, false);            

            RandNewSpawnDelay();
        }

        float dotSineMov = Mathf.Sin(_GameDuration * _BirdSineSpeed) * _BirdDeltaY;
        _TDot.anchoredPosition = 
            new Vector2(
                _StartBirdPosX,
                _StartBirdPosY + dotSineMov);        

        // checking for collisions
        foreach (ReversedFlappyBirdPipe pipe in _ObstaclesPool) {
            if (!pipe.gameObject.activeSelf) continue;

            if (pipe.IsColliding(_TDot.anchoredPosition)) {
                GameFailed();
                break;
            }
        }
    }

    ReversedFlappyBirdPipe ObstacleFactory() {
        ReversedFlappyBirdPipe obst = Instantiate(_ObstaclePrefab).GetComponent<ReversedFlappyBirdPipe>();
        obst.transform.SetParent(this.transform, false);
        return obst;
    }

    ReversedFlappyBirdPipe ObstacleInit(ReversedFlappyBirdPipe obstacle, float posX, float edgePosY, bool ceiling) {              

        if (!ceiling)
            edgePosY = edgePosY - obstacle.RTransform.sizeDelta.y;

        obstacle
            .RTransform
            .anchoredPosition = new Vector2(
                posX,
                edgePosY);
        return obstacle;
    }
}
